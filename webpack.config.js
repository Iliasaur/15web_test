"use strict";


const NODE_ENV = process.env.NODE_ENV || "development";
const API_URI = "https://api.codetabs.com/v1/proxy?quest=https://xkcd.com/";
const IMGS_COUNT = 2484;
const webpack = require("webpack");

module.exports = [

  { //--- ComicsViewer
    entry: {ComicsViewer: "./src/ComicsViewer.js"},
    
    output: {path: __dirname + "/assets/js", filename: "ComicsViewer.js", library: "ComicsViewer"},

    watch: NODE_ENV == "development",
    
    watchOptions: {aggregateTimeout: 300},
    
    devtool: NODE_ENV == "development" ? "source-map" : false,

    plugins: [new webpack.DefinePlugin({NODE_ENV: JSON.stringify(NODE_ENV), API_URI: JSON.stringify(API_URI), IMGS_COUNT: JSON.stringify(IMGS_COUNT)})],

    module: {rules: [{test: /\.js$/, loader: "babel-loader", options: {presets: ["@babel/preset-env"]}}, {test: /\.html$/, use: "raw-loader"}]}
  }
]

if (NODE_ENV == "production") {
  module.exports.forEach(function(item, i, arr) {
    item.plugins.push(new webpack.optimize.UglifyJsPlugin({compress: {warnings: false, drop_console: true, unsafe: true}}));
  })
}

