/******************************************************************************

														ОБЩЕПОЛЕЗНЫЕ УТИЛИТЫ
					
******************************************************************************/


class Utils {

	//------------- Парсит номер картинки
	static parseImageNumber() {
		const num = document.URL.split("/").reverse().find(part => !isNaN(part));
    return !isNaN(num) && num >= 0 && num <= IMGS_COUNT ? Number(num) : 0;
	}


	//------------- Запрос к API
	static apiGet(url) {
	  return new Promise(function(succeed, fail) {
	    const request = new XMLHttpRequest();
	    request.open("GET", url, true);
	    request.addEventListener("load", function() {
	      if (request.status < 400) {
	        succeed(JSON.parse(request.response));
	      } else {
	        fail(new Error("Request failed: " + request.statusText));
	      }
	    });

	    request.addEventListener("error", function() {
	      fail(new Error("Network error"));
	    });

	    request.send();
	  });
	}

}

export default Utils;
