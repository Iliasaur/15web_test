/******************************************************************************

														ВСПЛЫВАЮЩИЙ ДИАЛОГ
					
					  Здесь я бы использовал шаблонизатор Underscore
  					Но, увы, по правилам ТЗ использую нативный JS	

******************************************************************************/

import Template from './template.html';

class Message {
	
	//------------- Constructor
	constructor(options) {
		this.el = null;

		this.options = {container: null, title: "", text: "", code: "", buttons: []};
		if ((typeof(options) == "object") && (options != null)) {
			this.options.title = typeof(options.title) === "string" ? options.title : this.options.title;
			this.options.text = typeof(options.text) === "string" ? options.text : this.options.text;
			this.options.code = typeof(options.code) === "string" ? options.code : this.options.code;
			this.options.buttons = (typeof(options.buttons) === "object") && (options.buttons !== null) && (typeof(options.buttons.length) === "number") && (options.buttons.length > 0) ? options.buttons : this.options.buttons;
			this.options.container = (typeof(options.container) === "object") && (options.container !== null) ? options.container : null;
		}

		this.isPopup = this.options.container ? false : true;
		this.show();
	}


	//------------- Show
	show() {
		if (this.isPopup) {
			this.mask = document.createElement("div");
			this.mask.classList.add("mask");
			document.body.appendChild(this.mask);
		}

		this.el = document.createElement("div");
		this.el.classList.add("message");
		if (this.isPopup) {
			this.el.classList.add("message_popup");
		}

		this.el.innerHTML = Template;

		if (this.options.title) {
			this.el.getElementsByClassName("message__title")[0].append(this.options.title);
		} else {
			this.el.getElementsByClassName("message__body")[0].classList.add("message__title_empty");
		}
		if (this.options.code) { this.el.getElementsByClassName("message__code")[0].classList.add("message__code_" + this.options.code.toLowerCase()); }
		if (this.options.text) {
			this.el.getElementsByClassName("message__text")[0].innerText = this.options.text;
		} else {
			this.el.getElementsByClassName("message__text")[0].classList.add("message__text_empty");
		}


		if (this.options.buttons.length > 0) {
			let buttonsContainer = this.el.getElementsByClassName("message__buttons")[0];	
			for (let button of this.options.buttons) {
				let input = document.createElement("input");
				input.classList.add("button");
				input.classList.add("message__button");
				input.type = "button";
				input.value = button.title;
				buttonsContainer.appendChild(input);

				if (typeof(button.handler) == "function") {
	  			if (this.isPopup) {
		  			input.onclick = () => {
		  				this.hide();
		  				button.handler.call(this);
		  			}
		  		} else {
		  			input.onclick = button.handler.bind(this);
		  		}
	  		} else {
	  			if (this.isPopup) {
	  				input.onclick = this.hide.bind(this);
	  			}
	  		}
			}
		}

		if (this.options.container) {
			this.options.container.appendChild(this.el);
		} else {
  		document.body.appendChild(this.el);
  	}
	}


	//------------- Hide
	hide() {
		if (this.mask) { this.mask.remove(); }
		this.mask = null;

		if (this.el) { this.el.remove(); }
		this.el = null;
	}

}

export default Message;