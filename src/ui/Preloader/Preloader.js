/******************************************************************************

														ПРЕЛОАДЕР
					
					  Здесь я бы использовал шаблонизатор Underscore
  					Но, увы, по правилам ТЗ использую нативный JS	

******************************************************************************/

import Template from './template.html';


class Preloader {
	
	//------------- Constructor
	constructor(options) {
		this.el = null;

		this.options = {container: "", title: "", size: ""};
		if ((typeof(options) == "object") && (options != null)) {
			this.options.title = typeof(options.title) === "string" ? options.title : this.options.title;
			this.options.size = typeof(options.size) === "string" ? options.size : this.options.size;
			this.options.container = (typeof(options.container) === "object") && (options.container !== null) ? options.container : null;
		}

		this.isPopup = this.options.container ? false : true;
	}


	//------------- Show
	show() {
		if (this.isPopup) {
			this.mask = document.createElement("div");
			this.mask.classList.add("mask");
			document.body.appendChild(this.mask);
		}

		this.el = document.createElement("div");
		this.el.classList.add("preloader");
		if (this.isPopup) {
			this.el.classList.add("preloader_popup");
		}

		this.el.innerHTML = Template;

		if (this.options.size) {
			this.el.getElementsByClassName("circonf")[0].classList.add("circonf_" + this.options.size);
		}

		if (this.options.title) {
			this.el.getElementsByClassName("preloader__title")[0].innerText = this.options.title;
		} else {
			this.el.getElementsByClassName("preloader__title")[0].classList.add("preloader__title_empty");
		}

  	if (this.options.container) {
			this.options.container.appendChild(this.el);
		} else {
  		document.body.appendChild(this.el);
  	}
	}


	//------------- Hide
	hide() {
		if (this.mask) { this.mask.remove(); }
		this.mask = null;

		if (this.el) { this.el.remove(); }
		this.el = null;
	}

}

export default Preloader;