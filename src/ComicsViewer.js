import Utils from './Utils'
import Navigator from './Navigator'
import Preloader from './ui/Preloader'
import Message from './ui/Message'


class ComicsViewer {
  
  //------------- Конструктор
  constructor() {
    this.preloader = new Preloader();
    this.navigator = new Navigator(this.load.bind(this));

    this.busy(false);

    const imageNumber = Utils.parseImageNumber();
    this.load(imageNumber);

    window.onpopstate = (event) => {
      if (isNaN(event.state)) { return; }
      this.load();
    }
  }


  //------------- Busy
  busy(value) {
    if (value === undefined) { return this._busy; }
    if (typeof(value) === "boolean") {
      this._busy = value;
      this.navigator.setDisabled(value);
    }
  }


  //------------- Включаем / выключаем прелоадер
  togglePreloader(value) {
    if (value) {
      this.preloader.show();
    } else {
      this.preloader.hide();
    }
  }


  //------------- Alert PopUp
  alert(error) {
    if (this.message) {
      this.message.hide();
    }
 
    this.message = new Message({
      code: "error",
      text: error.message,
      buttons: [{title: "OK"}]
    });
  }


  //------------- Читаем данные картинки
  load() {
    this.togglePreloader(true);
    this.busy(true);

    const imageNumber = Utils.parseImageNumber();

    try {
      Utils.apiGet(API_URI + (imageNumber > 0 ? imageNumber + "/" : "") + "/info.0.json").then(
        response => {
          this.loadPicture(response);
        },
        error => {
          this.busy(false);
          this.togglePreloader(false);
          this.alert(error);
        }
      );
    } catch(error) {
      this.busy(false);
      this.togglePreloader(false);
      this.alert(error);
    }
  }


  //------------- Читаем картинку
  loadPicture(meta) {
    const image = new Image;
    image.src = meta.img;
    image.setAttribute("alt", meta.alt ? meta.alt.replace(/['"]+/g, '') : "");
    image.setAttribute("title", meta.alt ? meta.alt.replace(/['"]+/g, '') : "");
    
    image.onload = () => {
      this.togglePreloader(false);
      const cover = document.getElementById("img");
      cover.innerHTML = '';
      cover.appendChild(image);
      document.getElementById("title").innerText = meta.safe_title ? meta.safe_title : meta.title
      document.getElementById("date").innerText = (meta.day < 10 ? "0" : "") + meta.day + "." + (meta.month < 10 ? "0" : "") + meta.month + "." + meta.year
      document.getElementById("transcript").innerText = meta.transcript ? meta.transcript : ""
      this.busy(false);
    }
  }
}

export default ComicsViewer;