import Utils from './Utils'


class Navigator {
	

	//------------- Конструктор
  constructor(callback) {
  	this.callback = callback;
  	this.isDisabled = false;

    for (let button of document.getElementsByClassName("picture__button")) {
      button.addEventListener("click", this.navigate.bind(this));
      button.addEventListener("keyup", this.keyUp.bind(this))
    }
  }


  //------------- Set Disabled
  setDisabled(value) {
    this.isDisabled = value;
    this.toggleEnability();
  }


  //------------- Дизаблим кнопки
  toggleEnability() {
    for (let button of document.getElementsByClassName("picture__button")) {
      if (this.isDisabled) {
        button.classList.add("picture__button_disabled");
        continue;
      }
      const btnOffset = Number(button.dataset.offset);
      if (btnOffset === 8) {
        button.classList.remove("picture__button_disabled");
        continue;
      }

      const imageNumber = Utils.parseImageNumber();

      if (btnOffset <= 0) {
        if (imageNumber< 1) { button.classList.add("picture__button_disabled"); } else { button.classList.remove("picture__button_disabled"); }
      }
      if (btnOffset > 0) {
        if (imageNumber >= IMGS_COUNT) { button.classList.add("picture__button_disabled"); } else { button.classList.remove("picture__button_disabled"); }
      }
    }
  }


  //------------- Навигация
  navigate(event) {
    if (this.isDisabled) { return; }
    const offset = Number(typeof(event) === "object" ? event.target.dataset.offset : event);
    if (isNaN(offset)) { return; }

    const imageNumber = Utils.parseImageNumber();

    let num = -1;
    switch (offset) {
      case 0: num = 1; break;
      case 8: num = Math.floor(Math.random() * IMGS_COUNT); break;
      case 9: num = IMGS_COUNT; break;
      default: num = imageNumber + offset;
    }

    if (num < 0 || num > IMGS_COUNT) { return; }

    window.history.pushState(num, 'ComicsViewer', '/' + num);
    this.callback();
  }


  //------------- Навигация с клавиатуры
  keyUp(event) {
    switch (event.keyCode) {
      case 37: this.navigate(event.ctrlKey ? 0 : -1); break;
      case 39: this.navigate(event.ctrlKey ? 9 : 1); break;
    }
  }

}

export default Navigator;